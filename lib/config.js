var urlBase = "http://localhost/mpserver";

var apiList = {

	urlBase,

	wxLogin: `${urlBase}/Index/login`,

	decrypt: `${urlBase}/Index/decrypt`,

	index_banner: `${urlBase}/Index/banner`,

	get_shop: `${urlBase}/Shop/getWxUser`,

};

module.exports = apiList;
