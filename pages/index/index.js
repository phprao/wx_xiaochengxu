//index.js
//获取应用实例
const app = getApp();
var config  = require('../../lib/config.js');

Page({
  data: {
    imgUrls: [],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    latitude: "113.23",
    longitude: "23.16",
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }

    var self = this;
    // 获取轮播图
    wx.request({
      url: config.index_banner,
      success: function(res) {
        if(res.data.status == 0){
          var len = res.data.data.length;
          for(var i = 0 ; i < len ; i++){
            res.data.data[i] = config.urlBase + '/' + res.data.data[i];
          }
        }
        self.setData({
          imgUrls: res.data.data,
        });
      }
    });

    // wx.getLocation({
    //   type: 'wgs84',
    //   success: (res) => {
    //     this.setData({
    //       latitude: res.latitude // 经度
    //       longitude: res.longitude // 纬度
    //     })
    //   }
    // })
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }

  // wx.request({
  //   url: config.decrypt,
  //   data: {
  //     session_key: app.globalData.user.session_key,
  //     encrypted_data: e.detail.encryptedData,
  //     iv: e.detail.iv
  //   },
  //   method: 'POST',
  //   header: {
  //     'content-type': 'application/x-www-form-urlencoded'
  //   },
  //   success: function(res) {
  //     console.log(res.data)
  //     app.globalData.userInfo = res.data
  //     self.setData({
  //       userInfo: res.data,
  //       hasUserInfo: true
  //     })
  //   }
  // })
  
})
