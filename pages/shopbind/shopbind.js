const app = getApp();
var config  = require('../../lib/config.js');

app.globalData.player = {player_openid_mp: 'ovwIv5fP_OS_Kl1AGuPQKdgwNyUg'};

Page({
  data: {
    logs: [],
    is_bind: false,
    shop_info: null,
    openid: null,
    bind_id: null,
    bind_status: null,
    ad: null
  },
  onLoad: function () {
    var self = this;
    this.setData({
      openid: app.globalData.player.player_openid_mp
    });
    wx.request({
      url: config.get_shop,
      data: {
        openid: self.data.openid,
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function(res){
        console.log(res.data);
        var result = res.data;
        if(result.status == 0){
          if(typeof(result.data.info) != 'undefined'){
              self.setData({
                is_bind: true,
                bind_id: result.data.info.shop_id,
                bind_status: result.data.info.status
              });
          
              var status = result.data.info.status;
              if(status == 1){
                  // 申请中
                  // $('.outer-container').show();
                  // $('#shop_name').text(data.data.info.name);
                  // $('#shop_name').attr('contenteditable',"false");
                  // $('#shop_addr').text(data.data.info.address);
                  // $('#shop_addr').attr('contenteditable',"false");
                  // $('#shop_tel').text(data.data.info.tel);
                  // $('#shop_tel').attr('contenteditable',"false");
                  // $('#club_id').text(data.data.info.club_id);
                  // $('#club_id').attr('contenteditable',"false");
                  // $('#shop_img .img-src').attr('src', data.data.info.face_img);
                  // $('.img-background').hide();
                  // $('.img-src').show();
                  // $('#updoad_file_btn').attr('disabled', 'disabled');
                  // $('.part-five').hide();
                  // $('.p-waiting').show();
                  // $('.part-two button').text('取消申请');
                  // $('.part-two button').attr('data-action','cancel');
                  // $('.part-two').show();
              }else if(status == 3){
                  // 被拒绝
                  // $('.outer-container').show();
                  // $('#shop_name').text(data.data.info.name);
                  // $('#shop_name').attr('contenteditable',"false");
                  // $('#shop_addr').text(data.data.info.address);
                  // $('#shop_addr').attr('contenteditable',"false");
                  // $('#shop_tel').text(data.data.info.tel);
                  // $('#shop_tel').attr('contenteditable',"false");
                  // $('#club_id').text(data.data.info.club_id);
                  // $('#club_id').attr('contenteditable',"false");
                  // $('#shop_img .img-src').attr('src', data.data.info.face_img);
                  // $('.img-background').hide();
                  // $('.img-src').show();
                  // $('#updoad_file_btn').attr('disabled', 'disabled');
                  // $('.part-five').hide();
                  // $('.p-notice').hide();
                  // $('.part-two button').text('重新申请');
                  // $('.part-two button').attr('data-action','continue');
                  // $('.part-two').show();
                  // $('.p-notice-refuse .content').text(data.data.info.refuse_reason);
                  // $('.p-notice-refuse').show();
              }else if(status == 4){
                  // 已通过
                  // $('.outer-container-done').show();
                  // $('#shop_name_2').text(data.data.info.name);
                  // $('#shop_addr_2').text(data.data.info.address);
                  // $('#shop_tel_2').text(data.data.info.tel);
                  // $('#club_id_2').text(data.data.info.club_id);
                  // $('.club_name').text(data.data.info.club_name);
                  // $('.qrcode-img').attr('src', data.data.info.qrcode);
              }
          }else{
            self.setData({
              ad: config.urlBase + '/' + result.data.ad[0]
            });
          }
        }else{
          wx.showModal({
            title: "提示",
            content: result.msg,
            success: function(res) {
              
            }
          })
        }
      }
    });
  }
})
