//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function(e) {
    var self = this;

    app.globalData.userInfo = e.detail.userInfo;
    self.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })

    // wx.request({
    //   url: config.decrypt,
    //   data: {
    //     session_key: app.globalData.user.session_key,
    //     encrypted_data: e.detail.encryptedData,
    //     iv: e.detail.iv
    //   },
    //   method: 'POST',
    //   header: {
    //     'content-type': 'application/x-www-form-urlencoded'
    //   },
    //   success: function(res) {
    //     console.log(res.data)
    //     app.globalData.userInfo = res.data
    //     self.setData({
    //       userInfo: res.data,
    //       hasUserInfo: true
    //     })
    //   }
    // })
    
  }
})
